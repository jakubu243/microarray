import pandas as pd
import os
import numpy as np
import sys
import copy
import argparse


def find_optimal_samples(snp_samples, genotype_of_interest=None, used_cols=None, used_rows=None, max_col=-1):

    if used_cols and used_rows:
        #Picks from unaltered data the used samples

        arr_1_0 = df_sample_cols.iloc[:, used_cols]
        
        #rows/snp's without genotype_of_interest (the rows to search)
        no_genotype_of_interest  = np.where(~np.any(arr_1_0 == genotype_of_interest, axis=1))[0].tolist() #no_gen = np.where((~np.where(df == "0|0", True, False)))[0].tolist()
        with_genotype_of_interest = np.where(np.any(arr_1_0 == genotype_of_interest, axis=1))[0].tolist() #np.where(df == "0|0")[0].tolist()
        rows_with_genotype_of_interest = [x for x in no_genotype_of_interest if x in used_rows]

        m_snp_samples = df_sample_cols.iloc[no_genotype_of_interest, :].transpose().values.tolist()

        used_rows.update(rows_with_genotype_of_interest)

    else:
        m_snp_samples = copy.deepcopy(snp_samples)
    
    

    if used_cols is None and used_rows is None:
        used_cols = []
        used_rows = set()
        
    max_col_count = 0

    while True:
        if max_col_count == max_col:
            break

        # Sample with most genotype_of_interest's [index, number of genotype_of_interest's]
        highest_genotype_count = [0, 0]

        # Loop throgh samples
        for index, sample in enumerate(m_snp_samples):
            current_minor_genotype_count = sample.count(genotype_of_interest)

            if current_minor_genotype_count > highest_genotype_count[1]:
                highest_genotype_count[0] = index
                highest_genotype_count[1] = current_minor_genotype_count


        # Break if no more genotype_of_interest's left (GOOD)
        if highest_genotype_count[1] == 0:
            break
        
        # List of rows(snp's) index with genotype_of_interest in picked sample, reversed to avoid IndexError
        indices = [i for i, x in enumerate(m_snp_samples[highest_genotype_count[0]]) if x == genotype_of_interest]
        indices.reverse()
        
        # Remove rows(snp's) with genotype_of_interest already picked (MAYBE USE NP ARRAY, WHERE np.delete(m_snp_samples, [indices], axis= (0 or 1)))
        for sample in m_snp_samples:
            for index in indices:
                del sample[index]

        # Update used_rows and used_cols
        used_rows.update([i for i, x in enumerate(snp_samples_unaltered[highest_genotype_count[0]]) if x == genotype_of_interest])
        used_cols.append(highest_genotype_count[0])
        max_col_count += 1

        if genotype_of_interest == "1|1":
            cols_used_dict[max_col_count] = (used_cols.copy(), used_rows.copy())


    df1 = df_sample_cols.iloc[:, used_cols]
    return (used_cols, used_rows, df1)


def create_genotype_samples():
    def genotype_combinations(loops, cols1_1, x):
        for _ in range(loops):
            for cols_1_0 in range(x+1):
                genotype_samples.append([cols1_1, cols_1_0, x-cols_1_0])

            x += 1
            cols1_1 -= 1

    loops = cols1_1 = len(cols_used_dict)
    if cols1_1 > 20:
        loops = cols1_1 - (cols1_1 - 20)

    # if 1/1 found < cols 
    if cols1_1 < cols:
        genotype_combinations(loops, cols1_1, x=cols-cols1_1)

    # if 1/1 found = cols 
    else:
        genotype_combinations(loops - 1, cols1_1 - 1, x=1)


def stats(df):
    unique_value_counts = df.nunique(axis=1).value_counts(sort=False)
    print("Number of samples used:", len(df.columns))
    df = df.assign(genotype_count=df.nunique(axis=1))
    
    print("From", len(df_sample_cols), "snp's")
    if 3 in unique_value_counts.index:
        print("Snp's with 3 genotypes:", unique_value_counts[3])
    else:
        print("Snp's with 3 genotypes:", 0)

    if 2 in unique_value_counts.index:
        print("Snp's with 2 genotypes:", unique_value_counts[2])
    else:
        print("Snp's with 2 genotypes:", 0)

    if 1 in unique_value_counts.index:
        print("Snp's with 1 genotypes:", unique_value_counts[1])
    else:
        print("Snp's with 1 genotypes:", 0)

    return df

def is_valid_file(arg):
    if not os.path.exists(arg):
        parser.error("{arg} does not exist! Provide file path for tab seperated txt file".format(arg=arg))
    else:

        return arg
def check_max_cols_value(val):
    ival = int(val)
    if not 1 <= ival <= 2548:
        raise argparse.ArgumentTypeError("{ival} is an invalid integer value provide max column value between 1-2548".format(ival = ival))
    else:
        return ival


parser = argparse.ArgumentParser()
parser.add_argument('txt', type=is_valid_file, help="File path for tab seperated txt file")
parser.add_argument('cols', nargs="?", type=check_max_cols_value, help='Max columns/genotypes, integer between 1-100000 ', default=-1)
args = parser.parse_args()
file_input = args.txt
cols = args.cols

# Takes tab delimited file
bcf_file_path = "/storage/array_raw/BCF_FILES/combined.bcf.gz"
os.system(fr'bcftools view --regions-file {file_input} {bcf_file_path} | cut -f 1,2,4,5,10- | egrep -v "^##" | sed "1 s/#CHROM/Chr/;s/POS/MapInfo/;s/REF/Ref/;s/ALT/Alt/" > temp.csv')

df_input = "temp.csv"
file_output = "OUTPUT/optimal_samples.csv"

try:
    df = pd.read_csv(df_input, sep="\t", dtype=str)
except pd.errors.EmptyDataError as e:
    print(e, "\nThe txt file has to be tab seperated")
    os.remove("temp.csv")
    sys.exit(1)

os.remove("temp.csv")
df = df.replace('0|1', '1|0')

non_sample_columns_len = 4
df_non_sample_cols = df.iloc[:, :non_sample_columns_len]
df_sample_cols     = df.iloc[:, non_sample_columns_len:]

snp_samples_unaltered = df_sample_cols.transpose().values.tolist()

cols_used_dict = {}

a = find_optimal_samples(snp_samples=snp_samples_unaltered, genotype_of_interest="1|1", max_col=cols)
if cols == -1:
    b = find_optimal_samples(snp_samples=snp_samples_unaltered, genotype_of_interest="1|0", max_col=cols, used_cols=a[0], used_rows=a[1])
    c = find_optimal_samples(snp_samples=snp_samples_unaltered, genotype_of_interest="0|0", max_col=cols, used_cols=b[0], used_rows=b[1])
    
    result = stats(c[2])
    
    df_optimal = pd.concat([df_non_sample_cols , result], axis=1)
    df_optimal.to_csv(file_output, index=False)
    sys.exit()

# Create lists with amount of samples per genotype [1/1, 1/0, 0/0]  for example  [47, 0, 1], [47, 1, 0], [46, 0, 2], [46, 1, 1], [46, 2, 0] ... 
genotype_samples = []
create_genotype_samples()

sample_combinations = []
unique_counts = a[2].nunique(axis=1).value_counts(sort=False)
if 3 in unique_counts.index:
    three_genotypes = unique_counts[3]
else:
    three_genotypes = 0
if 2 in unique_counts.index:
    two_genotypes = unique_counts[2]
else:
    two_genotypes = 0
    
sample_combinations.append([a[2], [len(cols_used_dict), 0, 0], three_genotypes, two_genotypes])

for gs in genotype_samples:
    if gs[1] != 0 and gs[2] != 0:
        b = find_optimal_samples(snp_samples=snp_samples_unaltered, genotype_of_interest="1|0", max_col=gs[1], used_cols = cols_used_dict[gs[0]][0].copy(),
                             used_rows = cols_used_dict[gs[0]][1].copy())
        c = find_optimal_samples(snp_samples=snp_samples_unaltered, genotype_of_interest="0|0", max_col=gs[2], used_cols = b[0], used_rows = b[1])
    elif gs[1] == 0:
        c = find_optimal_samples(snp_samples=snp_samples_unaltered, genotype_of_interest="0|0", max_col=gs[2], used_cols = cols_used_dict[gs[0]][0].copy(),
                             used_rows = cols_used_dict[gs[0]][1].copy())
    elif gs[2] == 0:
        c = find_optimal_samples(snp_samples=snp_samples_unaltered, genotype_of_interest="1|0", max_col=gs[1], used_cols = cols_used_dict[gs[0]][0].copy(),
                             used_rows = cols_used_dict[gs[0]][1].copy())

    unique_counts = c[2].nunique(axis=1).value_counts(sort=False)
    if 3 in unique_counts.index:
        three_genotypes = unique_counts[3]
    else:
        three_genotypes = 0
    if 2 in unique_counts.index:
        two_genotypes = unique_counts[2]
    else:
        two_genotypes = 0
        
    # rows with all 3 genotypes (1/1, 1/0, 0/0)
    sample_combinations.append([c[2], gs, three_genotypes, two_genotypes])


sample_combinations.sort(key=lambda x: (x[2],x[3]))
sample_combinations.reverse()

result = stats(sample_combinations[0][0])

df_optimal = pd.concat([df_non_sample_cols , result], axis=1)
df_optimal.to_csv(file_output, index=False)

